--Seleccionar los 10 eventos con mas tweets.
select
e.name_event limit 10
from
review_events re, events e
where
	re.id = e.id
order by re.num_of_mentions


--Seleccionar tipo de evento
select *
from
event e
where
type_of_event= 'Tipo de evento'

-- Tweets por periodo de tiempo
SELECT 
    *
FROM
    Tweet t
WHERE
    date_publish_tweet > 'datetime1'
        AND date_publish_tweet < 'datetime2'

-- Obtener cantidad de tweets positivos o negativos

select
	count
    from
    DataTweetReview
    where 
    sensivity_rank=0 and  -- 0 si es positivo o 1 si se requieren negativos
    tweet_id=(
		select r.tweet_id
        from
        reference_t_es r
        where 
        event_id=(
			select e.id
            from tweets e
            where
            e.name_event = 'NombreEvento'
            ))
