CREATE DATABASE  IF NOT EXISTS `MASIVO_development` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `MASIVO_development`;
-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: MASIVO_development
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_publish_hashtag` datetime DEFAULT NULL,
  `official` tinyint(1) NOT NULL,
  `num_mentions_hashtag` int(11) DEFAULT '0',
  `hashtag_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashtag_content_UNIQUE` (`hashtag_content`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hashtags`
--

LOCK TABLES `hashtags` WRITE;
/*!40000 ALTER TABLE `hashtags` DISABLE KEYS */;
INSERT INTO `hashtags` VALUES (1,'2014-12-08 02:44:00',1,2,'#MysterylandCL','2014-12-07 23:47:33','2014-12-08 17:07:41'),(2,'2014-12-08 02:47:00',1,0,'#LollaCL','2014-12-07 23:47:55','2014-12-08 00:23:37'),(3,'2014-12-08 02:48:00',1,0,'#UltraChile2014','2014-12-07 23:48:45','2014-12-08 00:23:44'),(4,'2014-12-08 02:48:00',1,0,'#yovoyaULTRACHILE','2014-12-07 23:49:16','2014-12-08 00:23:53'),(5,'2014-12-08 02:49:00',1,0,'#BusesROCKOUT','2014-12-07 23:49:52','2014-12-08 00:24:01'),(6,'2014-12-08 02:49:00',1,1,'#RockOutfest','2014-12-07 23:50:12','2014-12-08 17:50:43'),(7,'2014-12-08 02:50:00',1,0,'#LegalizeFestival2014','2014-12-07 23:51:10','2014-12-08 00:25:43'),(8,'2014-12-08 02:51:00',1,0,'#MovistarPrimaveraFauna','2014-12-07 23:51:27','2014-12-08 00:25:35'),(9,'2014-12-08 02:51:00',1,9,'#Teletón2014','2014-12-07 23:51:43','2014-12-08 17:51:30'),(10,'2014-12-08 02:52:00',1,0,'#HoliFestivalCL','2014-12-07 23:52:35','2014-12-08 00:25:20'),(11,'2014-12-08 02:52:00',1,0,'#CapitalCitiesenChile','2014-12-07 23:52:56','2014-12-08 00:25:13'),(12,'2014-12-08 02:53:00',1,1,'#EdSheeranEnChile','2014-12-07 23:53:12','2014-12-08 17:04:49'),(13,'2014-12-08 02:53:00',1,0,'#SeVieneLaComicCon','2014-12-07 23:53:43','2014-12-08 00:24:58'),(14,'2014-12-08 02:53:00',1,0,'#Festigame2015','2014-12-07 23:54:15','2014-12-08 00:24:50'),(15,'2014-12-08 02:55:00',1,0,'#TeletónSomosTodos','2014-12-07 23:55:40','2014-12-08 00:24:42'),(16,'2014-12-08 02:56:00',1,0,'#Teletón2015','2014-12-07 23:56:15','2014-12-08 00:24:33'),(17,'2014-12-08 03:05:00',1,15,'#ParisParade','2014-12-08 00:05:16','2014-12-08 17:53:36'),(18,'2014-12-08 03:15:00',1,225,'#MasterChefMenudeReyes','2014-12-08 00:15:55','2014-12-08 17:54:01');
/*!40000 ALTER TABLE `hashtags` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER TriggerInsert_Hashtag before INSERT ON hashtags
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_mentions_hashtag<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = null;
      set new.created_at = now();
	  set new.updated_at = now();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER TriggerUpdate_Hashtag before UPDATE ON hashtags
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_mentions_hashtag<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
	  set new.updated_at = now();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER TriggerDelete_Hashtag before Delete ON hashtags
FOR each row
BEGIN 
 DELETE FROM hashtag_events WHERE hashtag_events.hashtag_id = old.id;
 DELETE FROM hashtag_tweets WHERE hashtag_tweets.hashtag_id = old.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-08 21:41:14
