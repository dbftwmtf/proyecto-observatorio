Drop trigger if exists TriggerInsert_tweet;
DELIMITER \\
CREATE TRIGGER TriggerInsert_tweet before INSERT ON tweets
FOR each row
BEGIN 
      declare msg varchar(255);
      if  new.num_favorite<0 or new.num_retweet<0 or new.num_reply<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if; 
      set new.created_at = now();
      set new.updated_at = now();
	  set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_Account;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Account before INSERT ON accounts
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('@', new.user_twitter) != 1 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "@....":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = now();
      set new.updated_at = now();
	  set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_Address;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Address before INSERT ON addresses
FOR each row
BEGIN 
      set new.updated_at = now();
      set new.created_at = now();
	  set new.id = null;
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_Advertise;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Advertise before INSERT ON advertises
FOR each row
BEGIN 
	  set new.date_end_advertise = null;
      set new.updated_at = now();
      set new.created_at = now();
	  set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_batch_deletion;
DELIMITER \\
CREATE TRIGGER TriggerInsert_batch_deletion before INSERT ON batch_deletions
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_tweets_delete< 0 or new.num_account_delete< 0 or new.num_evaluation_delete< 0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = now();
      set new.updated_at = now();
	  set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_Contact;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Contact before INSERT ON contacts
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('@', new.emails_contact) = 0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "****@***.***":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = now();
      set new.updated_at = now();
	  set new.id = null;
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_Contains_t_h;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Contains_t_h before INSERT ON contains_t_hs
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_DAE;
DELIMITER \\
CREATE TRIGGER TriggerInsert_DAE before INSERT ON data_account_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('https://twitter.com/', new.url_twitter_event) != 1 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "https://twitter.com/*****":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      if new.num_tweets<0 or new.num_photo_and_video<0 or new.num_following<0 or new.num_followers<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.updated_at = now();
      set new.created_at = now();
	  set new.id = null;
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_DETR;
DELIMITER \\
CREATE TRIGGER TriggerInsert_DETR before INSERT ON data_evaluation_real_times
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
      set new.id = null;
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_DTR;
DELIMITER \\
CREATE TRIGGER TriggerInsert_DTR before INSERT ON data_tweet_reviews
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
      set new.id = null;
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_EAE;
DELIMITER \\
CREATE TRIGGER TriggerInsert_EAE before INSERT ON evaluation_account_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_followers_start<0 or new.num_followers_end<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = null;
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_Evaluation;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Evaluation before INSERT ON evaluations
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_mentions<0 or new.num_mentions_positive<0 or new.num_mentions_negative<0 or new.num_user_active<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.date_end_evaluation = null;
      set new.created_at = now();
      set new.updated_at = now();
      set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_Event;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Event before INSERT ON events
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_turnout<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.ending_date_event = null;
	  set new.id = null;
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_Hashtag;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Hashtag before INSERT ON hashtags
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_mentions_hashtag<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = null;
      set new.created_at = now();
	  set new.updated_at = now();
END \\
DELIMITER ;


DROP TRIGGER IF EXISTS TriggerInsert_Organization;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Organization before INSERT ON organizations
FOR each row
BEGIN 
      set new.created_at = now();
	  set new.updated_at = now();
	  set new.id = null;
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_organize;
DELIMITER \\
CREATE TRIGGER TriggerInsert_organize before INSERT ON organizes
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_posses_h_e;
DELIMITER \\
CREATE TRIGGER TriggerInsert_posses_h_e before INSERT ON posses_h_es
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_RAE;
DELIMITER \\
CREATE TRIGGER TriggerInsert_RAE before INSERT ON review_account_events 
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_followers_start_review<0 or new.num_followers_end_review<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = null;
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_Review_event;
DELIMITER \\
CREATE TRIGGER TriggerInsert_Review_event before INSERT ON review_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if  new.num_of_mentions<0 or new.num_active_users<0 or new.num_mentions_positive_sum<0 or new.num_mentions_negative_sum<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.ending_date_review = null;
	  set new.id = null;
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerInsert_reference_a_t;
DELIMITER \\
CREATE TRIGGER TriggerInsert_reference_a_t before INSERT ON reference_a_ts
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerInsert_reference_t_e;
DELIMITER \\
CREATE TRIGGER TriggerInsert_reference_t_e before INSERT ON reference_t_es
FOR each row
BEGIN 
      set new.created_at = now();
      set new.updated_at = now();
END \\
DELIMITER ;






