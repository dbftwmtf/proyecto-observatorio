Drop trigger if exists TriggerUpdate_tweet;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_tweet before UPDATE ON tweets
FOR each row
BEGIN 
      declare msg varchar(255);
      if  new.num_favorite<0 or new.num_retweet<0 or new.num_reply<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if; 
      set new.created_at = old.created_at;
      set new.updated_at = now();
	  set new.id = old.id;
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_reference_t_e;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_reference_t_e before UPDATE ON reference_t_es
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_reference_a_t;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_reference_a_t before UPDATE ON reference_a_ts
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_Review_event;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Review_event before UPDATE ON review_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if  new.num_of_mentions<0 or new.num_active_users<0 or new.num_mentions_positive_sum<0 or new.num_mentions_negative_sum<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_RAE;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_RAE before UPDATE ON review_account_events 
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_followers_start_review<0 or new.num_followers_end_review<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_posses_h_e;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_posses_h_e before UPDATE ON posses_h_es
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_organize;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_organize before UPDATE ON organizes
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;

DROP TRIGGER IF EXISTS TriggerUpdate_Organization;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Organization before UPDATE ON organizations
FOR each row
BEGIN 
      set new.created_at = old.created_at;
	  set new.updated_at = now();
	  set new.id = old.id;
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_Hashtag;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Hashtag before UPDATE ON hashtags
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_mentions_hashtag<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
	  set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_Event;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Event before UPDATE ON events
FOR each row
BEGIN 
	  declare msg varchar(255);
      if new.num_turnout<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_Evaluation;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Evaluation before UPDATE ON evaluations
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_mentions<0 or new.num_mentions_positive<0 or new.num_mentions_negative<0 or new.num_user_active<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = old.created_at;
      set new.updated_at = now();
      set new.id = old.id;
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_EAE;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_EAE before UPDATE ON evaluation_account_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if new.num_followers_start<0 or new.num_followers_end<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos:', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.id = old.id;
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_DTR;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_DTR before UPDATE ON data_tweet_reviews
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
      set new.id = old.id;
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_DETR;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_DETR before UPDATE ON data_evaluation_real_times
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
      set new.id = old.id;
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_DAE;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_DAE before UPDATE ON data_account_events
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('https://twitter.com/', new.url_twitter_event) != 0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "https://twitter.com/*****":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      if new.num_tweets<0 or new.num_photo_and_video<0 or new.num_following<0 or new.num_followers<0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , numeros deben ser positivos":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
	  set new.updated_at = now();
      set new.created_at = old.created_at;
	  set new.id = old.id;
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_Contains_t_h;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Contains_t_h before UPDATE ON contains_t_hs
FOR each row
BEGIN 
      set new.created_at = old.created_at;
      set new.updated_at = now();
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_Contact;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Contact before UPDATE ON contacts
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('@', new.emails_contact) = 0 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "****@***.***":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = old.created_at;
      set new.updated_at = now();
	  set new.id = old.id;
END \\
DELIMITER ;


Drop trigger if exists TriggerUpdate_Advertise;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Advertise before UPDATE ON advertises
FOR each row
BEGIN 
      set new.updated_at = now();
      set new.created_at = old.created_at;
	  set new.id = old.id;
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_Address;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Address before UPDATE ON addresses
FOR each row
BEGIN 
      set new.updated_at = now();
      set new.created_at = old.created_at;
	  set new.id = old.id;
END \\
DELIMITER ;

Drop trigger if exists TriggerUpdate_Account;
DELIMITER \\
CREATE TRIGGER TriggerUpdate_Account before UPDATE ON accounts
FOR each row
BEGIN 
      declare msg varchar(255);
      if LOCATE('@', new.user_twitter) != 1 then
        set msg = concat('TriggerInsert_Account: Mal formato de entrada , requiere "@....":', cast(new.id as char));
        signal sqlstate '45000' set message_text = msg; 
      end if;
      set new.created_at = old.created_at;
      set new.updated_at = now();
	  set new.id = old.id;
END \\
DELIMITER ;


